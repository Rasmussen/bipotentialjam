# Модель бипотенциальной ямы

## Внешний вид
![Графическое окно](http://storage9.static.itmages.ru/i/16/0109/h_1452358843_2012642_8b7f4c0818.png)

## Вид модели Simulink
![Модель Simulink](http://storage2.static.itmages.ru/i/16/0109/h_1452358917_8399804_ec6a53a07b.png)

## Как запускать симуляцию
1. Скачать архив, расположенный по [ссылке](https://bitbucket.org/Rasmussen/bipotentialjam/raw/915276187416db50a023a647f530604ca1a11085/bipotentialJam.zip) и распаковать его
2. Запустить матлаб
3. Перейти в папку, которая содержит файлы bipotentialJam.mdl, gui.m и gui_utf.m
4. В командном окне (command window) запустить команду:
  **gui** (*для Windows*) или
  **gui_utf** (*для Linux и OS X* )

## Требования
* Matlab не старше версии R14
* Simulink не страше версии 6.0
